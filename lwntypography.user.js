// ==UserScript==
// @name          LWN Typography script
// @namespace     tag:sztanpet@gmail.com,2011:lwntypo
// @description   Makes LWN not look so getho
// @include       http://lwn.net/Articles/*
// @include       https://lwn.net/Articles/*
// ==/UserScript==

var style =
  "body {\n" +
    "  background-color: #ede9d0 !important;\n" +
    "  font: 0.75em Tahoma, Arial, Helvetica, sans-serif;\n" +
    "  color: #666359;\n" +
    "  line-height: 18px;\n" +
  "}\n" +
  "h1, h2 {\n" +
    "  font-weight: bold;\n" +
    "  color: #48463C;\n" +
    "  text-trasnform: uppercase;\n" +
    "  margin: 10px 0;\n" +
  "}\n" +
  "h1 {\n" +
    "  font-size: 1.85em;\n" +
  "}\n" +
  "h2 {\n" +
    "  font-size: 1em;\n" +
  "}\n" +
  "a:link, a:active, a:visited {\n" +
    "  color: #0100F0;\n" +
    "  text-decoration: none;\n" +
  "}\n" +
  "a:hover {\n" +
    "  text-decoration: underline;\n" +
  "}\n" +
  "p {\n" +
    "  text-align: justify;\n" +
  "}\n" +
  "p img[align=right] {\n" +
    "  margin: 0px 0px 10px 10px;\n" +
    "  clear: right;\n" +
  "}\n" +
  "p img[align=left] {\n" +
    "  margin: 0px 10px 10px 0px;\n" +
    "  clear: left;\n" +
  "}\n" +
  "div.PageHeadline {\n" +
    "  text-align: left;\n" +
  "}\n" +
  "table td.MidColumn {\n" +
    "  background-color: #fdfcf5;\n" +
    "  padding: 15px;\n" +
  "}\n"
;
GM_addStyle( style );

// an empty <td> element thats there because of the right column
var element = document.getElementsByClassName('MidColumn')[0].parentNode.parentNode.children[0];
element.removeChild( element.children[1] );

// move the left menu after the mid column and change its class
var leftmenu = document.getElementsByClassName('LeftColumn')[0];
var newtr = document.createElement('tr');
document.getElementsByClassName('MidColumn')[0].parentNode.parentNode.appendChild( newtr );
newtr.appendChild( leftmenu );
leftmenu.setAttribute('class', 'MidColumn');

// move the right column after the mid column, we now have no right column so the content has more space
var rightmenu = document.getElementsByClassName('RightColumn')[0];
var newtr = document.createElement('tr');
document.getElementsByClassName('MidColumn')[0].parentNode.parentNode.appendChild( newtr );
newtr.appendChild( rightmenu );
rightmenu.setAttribute('class', 'MidColumn');
